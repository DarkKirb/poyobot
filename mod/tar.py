"""This module exists for other modules to create and upload tar files \
that are automatically split when uploaded"""
from utils import Cog
import tempfile
import aiofiles
import os
import tarfile
import discord
import lzma
import io


__author__ = "Dark Kirb"
__license__ = "BSD-2clause"
__website__ = "https://gitlab.com/DarkKirb/poyobot/blob/master/mod/tar.py"
__version__ = "1.0"
dependencies = []


class TARInstance:
    def __init__(self, dest, tarfname):
        self.dest = dest
        self.tarfname = tarfname
        self.files = []

    async def __aenter__(self):
        self._tempdir = tempfile.TemporaryDirectory()
        self._tempdir_enter = self._tempdir.__enter__
        self._tempdir_exit = self._tempdir.__exit__
        self.tempdir = self._tempdir_enter()
        return self

    async def __aexit__(self, *ex):
        outname = self.tarfname + ".tar.xz"
        tfn = self.tarfname
        tfn = os.path.join(self.tempdir, tfn)
        tfn += ".tar"
        tf = tarfile.open(tfn, "w")
        for fname in self.files:
            tf.add(fname)
        tf.close()
        async with aiofiles.open(tfn, "rb") as inf:
            with lzma.LZMAFile(f"{tfn}.xz", "wb") as outf:
                data = await inf.read(8 * 1024 * 1024)
                while data != b"":
                    outf.write(data)
                    data = await inf.read(8 * 1024 * 1024)
        async with aiofiles.open(f"{tfn}.xz", "rb") as inf:
            chunk_count = 1
            data = await inf.read(8 * 1000 * 1000)
            if len(data) < 8 * 1000 * 1000:
                await self.dest.send(file=discord.File(io.BytesIO(data),
                                                       outname))
            else:
                while len(data):
                    no = str(chunk_count).zfill(3)
                    chunk_count += 1
                    await self.dest.send(file=discord.File(io.BytesIO(data),
                                                        f"{outname}.{no}"))
                    data = await inf.read(8 * 1000 * 1000)

        self._tempdir_exit(*ex)

    def open(self, fname, mode="w"):
        fname = os.path.join(self.tempdir, fname)
        if fname not in self.files:
            self.files.append(fname)
        return aiofiles.open(fname, mode)

    def mkdir(self, dname):
        os.makedirs(os.path.join(self.tempdir, dname), exist_ok=True)

    def __getitem__(self, fname):
        return self.open(fname)


class TAR(Cog):
    create = TARInstance


def setup(bot):
    global cog
    cog = TAR(bot)
