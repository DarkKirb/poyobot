"""The IO module for reading/writing data from/to chat"""
from utils import Cog, command
from discord.ext import commands


__author__ = "Dark Kirb"
__license__ = "BSD-2clause"
__website__ = "https://gitlab.com/DarkKirb/poyobot/blob/master/mod/io.py"
__version__ = "0.1"
dependencies = []


async def read_input(bot, tgt, user, message, validator=lambda m: True,
                     validator_hint=None):
    await tgt.send(message)
    while True:
        msg = await bot.wait_for('message', check=lambda m: (
            m.channel == tgt and m.author == user))
        if not validator(msg):
            await tgt.send(validator_hint)
        else:
            return msg


class IO(Cog):
    pass


def setup(bot):
    global cog
    cog = IO(bot)
