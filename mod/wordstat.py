'''"Time since last accident"-like module for individual words'''
from utils import Cog, command
from discord.ext import commands
from discord import http
import discord
from datetime import datetime
from discord.utils import find
import database
from database import Query
import tabulate
import aiohttp


__author__ = "Charlotte D"
__license__ = "BSD-2clause"
__website__ = "https://gitlab.com/DarkKirb/poyobot/blob/master/mod/wordstat.py"
__version__ = "1.0"
dependencies = []


table = database.db.table("vorestats")


def format_timediff(diff):
    seconds = int(diff.total_seconds())
    outstr = ""
    if seconds % 60:
        outstr = f"{seconds%60}s{outstr}"
    minutes = seconds // 60
    if minutes % 60:
        outstr = f"{minutes%60}m{outstr}"
    hours = minutes // 60
    if hours % 24:
        outstr = f"{hours%24}h{outstr}"
    days = hours // 24
    if days:
        outstr = f"{days}d{outstr}"
    return outstr


class WordStat(Cog):
    def __init__(self, bot):
        super().__init__(bot)
        Entry = Query()
        entries = table.search(Entry.type == "last_time")
        if entries == []:
            self.last_vore_time = 0
        else:
            self.last_vore_time = datetime.utcfromtimestamp(entries[0]["time"])
        entries = table.search(Entry.type == "users")
        if entries == []:
            self.voreusers = {}
            table.insert({"type":"users", "users":{}})
        else:
            self.voreusers = entries[0]["users"]

    async def on_message(self, message):

        if message.guild.id != 132720341058453504:
            return
        if ("vore" not in message.content.lower() and
                "voring" not in message.content.lower()):
            return
        if self.last_vore_time == 0:
            self.last_vore_time = message.created_at
            return
        channel = find(lambda c: c.id == 186342269274554368,
                       message.guild.text_channels)
        timediff = message.created_at - self.last_vore_time
        self.last_vore_time = message.created_at
        if str(message.author.id) not in self.voreusers:
            self.voreusers[str(message.author.id)] = [str(message.id)]
        else:
            self.voreusers[str(message.author.id)].append(str(message.id))
        Entry = Query()
        table.update({"users": self.voreusers}, Entry.type == "users")
        Entry = Query()
        table.update({
            "time": (self.last_vore_time - datetime.utcfromtimestamp(0))
            .total_seconds()
        }, Entry.type == "last_time")
        await channel.send(f"{message.author.mention} has said the v-word!\
 This server has been without it for {format_timediff(timediff)} before.")

    @command()
    async def vstats(self, ctx):
        """Shows stats about how often the v-word was used"""
        sorted_stats = sorted(list(self.voreusers.items()),
                              key=lambda x: len(x[1]),
                              reverse=True)
        sorted_data = [["User", "Amount", "Last"]]
        for uid, msgs in sorted_stats[:5]:
            user = find(lambda m: m.id == int(uid), ctx.message.guild.members)
            if user is None:
                uname = f"<@{uid}>"
            else:
                uname = f"{user.name}#{user.discriminator}"
            count = len(msgs)
            most_recent = int(max(msgs, key=lambda m:int(m)))
            created = discord.Object(most_recent).created_at
            last_time = created.isoformat()
            sorted_data.append([uname, count, last_time])

        formatted = tabulate.tabulate(sorted_data, headers="firstrow")
        await ctx.send(f"```\n{formatted}\n```")


def setup(bot):
    global cog
    cog = WordStat(bot)
