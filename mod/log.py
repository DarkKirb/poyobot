"""This is a moderation module meant to log user activity such as avatars,
nicknames, deleted and edited messages, ..."""
from utils import Cog, command, group, is_admin, NotAdminError
from discord.ext import commands
from discord.utils import find
import discord
import collections
import database
from database import Query
from . import io


__author__ = "Dark Kirb"
__license__ = "BSD-2clause"
__website__ = "https://gitlab.com/DarkKirb/poyobot/blob/master/mod/log.py"
__version__ = "0.1"
dependencies = ["io"]


table = database.db.table("log")


class Log(Cog):
    auto_enable = False

    def __init__(self, bot):
        super().__init__(bot)
        self.messages = collections.deque()

    async def can_monitor_server(self, id):
        Entry = Query()
        doc = table.search(Entry.guild == id)
        if doc == []:
            return False
        return doc[0]["enabled"]

    async def can_monitor_event(self, id, event):
        Entry = Query()
        doc = table.search(Entry.guild == id)[0]
        if event + "_enabled" not in doc:
            return False
        return doc[event + "_enabled"]

    async def get_event_channel(self, id, event):
        Entry = Query()
        doc = table.search(Entry.guild == id)[0]
        guild = find(lambda x: x.id == id, self.bot.guilds)
        if event + "_channel" in doc:
            channel_id = doc[event + "_channel"]
        else:
            channel_id = doc["channel"]
        channel = find(lambda x: x.id == channel_id, guild.channels)
        return channel

    async def on_message(self, message):  # add messages into cache
        if not await self.can_monitor_server(message.guild.id):
            return
        self.messages.append((message, message.content))
        if len(self.messages) > 10000:
            self.messages.popleft()

    async def on_raw_message_delete(self, event):
        if not await self.can_monitor_server(event.guild_id):
            return
        try:
            msg = find(lambda x: x[0].id == event.message_id, self.messages)
            if msg is None:
                raise ValueError("not found")
            index = self.messages.index(msg)
            del self.messages[index]
            msg = msg[0]
        except ValueError as e:
            print(e)
            index = None
            msg = None
        if not await self.can_monitor_event(event.guild_id, "delete"):
            return
        channel = await self.get_event_channel(event.guild_id, "delete")
        if msg is None:
            src_channel = find(lambda x: x.id == event.channel_id,
                               channel.guild.channels)
            e = discord.Embed()
            e.color = 0xFF0000
            e.title = "Message deleted in " + src_channel.name
        else:
            e = discord.Embed()
            e.color = 0xFF0000
            e.title = "Message deleted in " + msg.channel.name
            e.add_field(name="Contents", value=msg.content)
            e.add_field(name="Name",
                        value=f"{msg.author.name}#{msg.author.discriminator}")
        await channel.send(embed=e)

    async def on_raw_bulk_message_delete(self, event):
        if not await self.can_monitor_server(event.guild_id):
            return
        for msg in event.message_ids:
            msg = find(lambda x: x[0].id == msg, self.messages)
            if msg is None:
                continue
            index = self.messages.index(msg)
            del self.messages[index]
        if not await self.can_monitor_event(event.guild_id, "delete"):
            return
        channel = await self.get_event_channel(event.guild_id, "delete")
        src_channel = find(lambda x: x.id == event.channel_id,
                           channel.guild.channels)
        e = discord.Embed()
        e.color = 0xFF0000
        e.title = "Bulk delete in " + msg.channel.name
        e.set_footer(text=str(len(event.message_ids)) + " messages deleted")
        await channel.send(embed=e)

    async def on_raw_message_edit(self, payload):
        if "content" not in payload.data:
            return
        # find message id
        msg = find(lambda x: x[0].id == payload.message_id, self.messages)
        if msg is None:
            return
        if not await self.can_monitor_server(msg[0].guild.id):
            return
        if not await self.can_monitor_event(msg[0].guild.id, "edit"):
            return
        channel = await self.get_event_channel(msg[0].guild.id, "edit")

        e = discord.Embed()
        e.color = 0xFF
        e.title = "Edit in " + msg[0].channel.name
        e.add_field(name="Before", value=msg[1])
        e.add_field(name="Current", value=payload.data["content"])
        idx = self.messages.index(msg)
        self.messages[idx] = (msg[0], payload.data["content"])
        await channel.send(embed=e)

    async def on_member_join(self, member):
        if not await self.can_monitor_server(member.guild.id):
            return
        if not await self.can_monitor_event(member.guild.id, "join"):
            return
        channel = await self.get_event_channel(msg.guild.id, "join")

        e = discord.Embed()
        e.color = 0xFF00
        e.title = "Member joined"
        e.add_field(name="Name", value=f"{member.name}#{member.discriminator}")
        e.add_field(name="ID", value=member.id)
        e.add_field(name="Created at", value=member.created_at.isoformat())
        await channel.send(embed=e)

    async def on_member_remove(self, member):
        if not await self.can_monitor_server(member.guild.id):
            return
        if not await self.can_monitor_event(member.guild.id, "leave"):
            return
        channel = await self.get_event_channel(member.guild.id, "leave")

        e = discord.Embed()
        e.color = 0xFF0000
        e.title = "Member left"
        e.add_field(name="Name", value=f"{member.name}#{member.discriminator}")
        e.add_field(name="ID", value=member.id)
        e.add_field(name="Created at", value=member.created_at.isoformat())
        e.add_field(name="Joined at", value=member.joined_at.isoformat())
        await channel.send(embed=e)

    async def on_member_update(self, before, after):
        if not await self.can_monitor_server(before.guild.id):
            return
        if before.nick != after.nick and await self.can_monitor_event(
                before.guild.id, "rename"):
            channel = await self.get_event_channel(before.guild.id, "rename")
            e = discord.Embed()
            e.title = "Nickname changed"
            e.add_field(name="Name",
                        value=f"{before.name}#{before.discriminator}")
            e.add_field(name="Before", value=before.nick)
            e.add_field(name="After", value=after.nick)
            await channel.send(embed=e)
        if ((before.name != after.name) or (before.discriminator !=
                after.discriminator)) and await self.can_monitor_event(
                before.guild.id, "rename_id"):
            channel = await self.get_event_channel(before.guild.id,
                                                   "rename_id")
            e = discord.Embed()
            e.title = "Identity changed"
            e.add_field(name="ID", value=before.id)
            e.add_field(name="Before",
                        value=f"{before.name}#{before.discriminator}")
            e.add_field(name="After",
                        value=f"{after.name}#{after.discriminator}")
            await channel.send(embed=e)
        if before.avatar != after.avatar and await self.can_monitor_event(
                before.guild.id, "avatar"):
            channel = await self.get_event_channel(before.guild.id, "avatar")
            e = discord.Embed()
            e.title = "Avatar changed"
            e.add_field(name="Name",
                        value=f"{after.name}#{after.discriminator}")
            e.set_image(url=after.avatar_url)
            await channel.send(embed=e)

    @group()
    async def logger(self, ctx):
        """Command for configuring the logger"""
        if not await is_admin(ctx.message.guild, ctx.message.author):
            raise NotAdminError()

    @logger.command()
    async def enable(self, ctx):
        """Enables the logger on the server. No data will be collected/sent \
unless this command has been run first"""
        await self.handle_enable(ctx.message.guild.id)
        await ctx.send("👌")

    async def handle_enable(self, guild_id):
        Entry = Query()
        doc = table.search(Entry.guild == guild_id)
        if doc == []:
            create = True
            doc = {"guild": guild_id,
                   "enabled": True,
                   "_id": database.gen_id()}
            table.insert(doc)
        else:
            doc = doc[0]
            doc["enabled"] = True
            table.update(doc, Entry._id == doc["_id"])

    @logger.command()
    async def disable(self, ctx):
        """Disables the logger on the server. This means that new data will \
no longer be collected/sent after the command has been run"""
        await self.handle_disable(ctx.message.guild.id)
        await ctx.send("👌")

    async def handle_disable(self, guild_id):
        Entry = Query()
        doc = table.search(Entry.guild == guild_id)
        if doc != []:
            doc = doc[0]
            doc["enabled"] = False
            table.update(doc, Entry._id == doc["_id"])

    @logger.command()
    async def enable_log(self, ctx, kind: str, channel: discord.TextChannel):
        """Enables logging a specific kind of event. Available are delete, \
edit, join, leave, rename, rename_id and avatar, although more events might \
become available in the future."""
        await self.handle_enable_log(ctx, ctx.message.guild.id, kind,
                                     channel.id)
        await ctx.send("👌")

    async def handle_enable_log(self, ctx, guild_id, kind, channel_id):
        Entry = Query()
        doc = table.search(Entry.guild == guild_id)
        print(doc)
        if doc == [] or not doc[0]["enabled"]:
            await ctx.send("You need to enable the log first!")
            return
        doc = doc[0]
        doc[kind + "_enabled"] = True
        doc[kind + "_channel"] = channel_id
        table.update(doc, Entry._id == doc["_id"])

    @logger.command()
    async def disable_log(self, ctx, kind: str):
        """Disables logging a specific kind of event."""
        await self.handle_disable_log(ctx, ctx.message.guild.id, kind)
        await ctx.send("👌")

    async def handle_disable_log(self, ctx, guild_id, kind):
        Entry = Query()
        doc = table.search(Entry.guild == guild_id)
        if doc == [] or not doc[0]["enabled"]:
            await ctx.send("You need to enable the log first!")
            return
        doc = doc[0]
        doc[kind + "_enabled"] = False
        table.update(doc, Entry._id == doc["_id"])

    async def on_setup(self, ctx):
        msg = await io.read_input(self.bot, ctx.message.channel, ctx.author,
                                  f"Do you want to enable logging for your \
server?",
                                  lambda m: "yes" in m.content or \
                                  "no" in m.content or \
                                  "default" in m.content,
                                  "Answer with yes, no or default")
        if "yes" in msg.content:
            await self.handle_enable(ctx.message.guild.id)
        else:
            return

        for cur_log in "delete edit join leave rename rename_id " \
                        "avatar".split(" "):
            msg = await io.read_input(self.bot, ctx.message.channel,
                                      ctx.author, f"Where do you want \
{cur_log} logs to go to? (channel mention or default to disable)",
                                      lambda m: "default" in m.content or
                                      m.channel_mentions != [],
                                      "Channel mention or default")
            if "default" in msg.content:
                continue
            await self.handle_enable_log(msg.channel,
                                            ctx.message.guild.id,
                                            cur_log,
                                            msg.channel_mentions[0].id)


def setup(bot):
    global cog
    cog = Log(bot)
