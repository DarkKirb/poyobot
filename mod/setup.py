"""Module used to setup poyobot on a server"""
from utils import Cog, command, is_admin
from discord.ext import commands
from . import io
import module


__author__ = "Dark Kirb"
__license__ = "BSD-2clause"
__website__ = "https://gitlab.com/DarkKirb/poyobot/blob/master/mod/setup.py"
__version__ = "1.0"
dependencies = ["io"]


class Setup(Cog):
    @command()
    async def setup(self, ctx):
        if not await is_admin(ctx.guild, ctx.author):
            return
        await ctx.send("Welcome to poyobot's setup! This setup will ask you \
a few questions to set up poyobot easily. If you want to keep the current \
value/skip the current question enter \"default\"")
        while True:
            enabled_mods = []
            for modname, mod in self.bot.extensions.items():
                if mod.cog.check_once(ctx):
                    enabled_mods.append(modname[4:])
            msg = await io.read_input(self.bot, ctx.message.channel,
                                      ctx.author, f"Do you want to disable \
any of these modules server-wide?: \n{', '.join(enabled_mods)}")
            if msg.content == "default":
                break
            for mod in msg.content.split(","):
                mod = mod.strip()
                await module.cog.set_perm(ctx, mod, False, False, True, False,
                                          False)

        while True:
            disabled_mods = []
            for modname, mod in self.bot.extensions.items():
                if not mod.cog.check_once(ctx):
                    disabled_mods.append(modname[4:])
            msg = await io.read_input(self.bot, ctx.message.channel,
                                      ctx.author, f"Do you want to enable any \
of these modules server-wide?: \n{', '.join(disabled_mods)}")
            if msg.content == "default":
                break
            for mod in msg.content.split(","):
                mod = mod.strip()
                await module.cog.set_perm(ctx, mod, False, False, True, False,
                                          True)

        for modname, mod in self.bot.extensions.items():
            if mod.cog.check_once(ctx):
                await mod.cog.on_setup(ctx)


def setup(bot):
    global cog
    cog = Setup(bot)
