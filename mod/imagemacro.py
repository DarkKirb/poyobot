'''Applies a filter to an image or an avatar'''
from utils import Cog, command
from discord.ext import commands
from functools import wraps
from . import imageloader
from PIL import Image
from io import BytesIO
import discord


__author__ = "Charlotte D"
__license__ = "BSD-2clause"
__website__ = "https://gitlab.com/DarkKirb/poyobot/blob/master/mod/" \
              "imagemacro.py"
__version__ = "1.0"
dependencies = ["imageloader"]


def image_filter(*pastes, avatar=False, fgpastes=None):
    def decorator(f):
        @command()
        @wraps(f)
        async def wrapper(self, ctx, *args, **kwargs):
            fg = await f(self, ctx, *args, **kwargs)
            if fg is None:
                return
            if not avatar or ctx.author.avatar_url is None:
                bg = await imageloader.cog.get_latest_image(ctx.channel)
            else:
                bg = await imageloader.cog.fetch_image(ctx.author.avatar_url
                                                       .replace("webp", "png"))
                if bg is None:
                    bg = await imageloader.cog.get_latest_image(ctx.channel)
            tgt = Image.new("RGBA", fg.size)
            for x_offset, y_offset, width, height in pastes:
                tgt.paste(bg.resize((width, height)), (x_offset, y_offset))
            tgt.paste(fg, (0, 0), fg)
            if fgpastes:
                for x_offset, y_offset, width, height in fgpastes:
                    tgt.paste(bg.resize((width, height)), (x_offset, y_offset))
            output = BytesIO()
            tgt.save(output, "PNG")
            output.seek(0)
            await ctx.send(file=discord.File(output, f"{f.__name__}.png"))
        return wrapper
    return decorator


class ImageMacro(Cog):
    @image_filter((180, 0, 512, 512), avatar=True)
    async def dead(self, ctx):
        return Image.open("data/dead.png")

    @image_filter((271, 17, 400, 400), (190, 387, 128, 128), avatar=True)
    async def dont(self, ctx):
        return Image.open("data/dont.png")


def setup(bot):
    global cog
    cog = ImageMacro(bot)
