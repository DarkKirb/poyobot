"""This module contains various moderation utilities for r/Kirby"""
from utils import Cog, is_mod, command, group
import discord
from discord.ext import commands
import database
from database import Query
import asyncio
from datetime import datetime, timedelta
import re

__author__ = "Dark Kirb"
__license__ = "BSD-2-clause"
__website__ = "https://gitlab.com/DarkKirb/poyobot/blob/master/mod/mod.py"
__version__ = "1.0"
dependencies = []

table = database.db.table("mod_log")


class Mod(Cog):
    def __init__(self, bot):
        super().__init__(bot)
        self.scheduled_scheduler = False

    def schedule(self):
        self.scheduled_scheduler = False
        for ent in table:
            if "expires" in ent and ent["expires"] != "":
                delay = datetime.fromtimestamp(ent["expires"]) - \
                        datetime.utcnow()
                delay = delay.total_seconds()
                if delay <= 0:
                    asyncio.get_event_loop().call_soon(self.cancel_handler,
                                                       ent)
                elif delay < 60 * 60 * 4:
                    asyncio.get_event_loop().call_later(
                        delay,
                        self.cancel_handler,
                        ent
                    )
                elif not self.scheduled_scheduler:
                    asyncio.get_event_loop().call_later(60 * 60 * 4,
                                                        self.schedule)
                    self.scheduled_scheduler = True

    def cancel_handler(self, ent):
        async def cancel():
            # get guild
            guild = self.bot.get_guild(132720341058453504)
            affected_channel = self.bot.get_channel(ent["channel"])
            if affected_channel is None:
                # nothing to do
                return
            member = guild.get_member(ent["member"])
            if member is None:
                # nothing to do
                return
            table.update({"expires": ""}, Query().id == ent["id"])
            await self.add_entry({
                "mod": 379940375252434955,
                "member": ent["member"],
                "channel": ent["channel"],
                "verb": ent["verb"],
                "kind": "allow",
                "reason": "Time is up!"
            })
            overwrite = affected_channel.overwrites_for(member)
            if ent["verb"] == "send":
                overwrite.send_messages = None
            elif ent["verb"] == "react":
                overwrite.add_reactions = None
            elif ent["verb"] == "external":
                overwrite.external_emojis = None
            elif ent["verb"] == "embed":
                overwrite.embed_links = None
                overwrite.attach_files = None
            if overwrite.is_empty():
                await affected_channel.set_permissions(member, overwrite=None,
                                                       reason="Timer is over")
            else:
                await affected_channel.set_permissions(member,
                                                       overwrite=overwrite,
                                                       reason="Timer is over")

        asyncio.ensure_future(cancel())

    async def add_entry(self, ent):
        table.insert(dict(**ent, id=database.gen_id()))
        guild = self.bot.get_guild(132720341058453504)
        channel = self.bot.get_channel(307764076627623947)
        mod = guild.get_member(ent["mod"])
        member = guild.get_member(ent["member"])
        where = self.bot.get_channel(ent["channel"])
        msg = f"{mod.mention} {ent['kind']}ed {member.mention} to \
{ent['verb']} in {where.mention} with reason: {ent['reason']}"
        m = await channel.send(msg)
        # send dm if possible
        if ent["kind"] == "disallow":
            msg = f"You got your permission to {ent['verb']} in \
#{where.name} on r/Kirby revoked. Reason: {ent['reason']}"
            try:
                await member.send(msg)
            except:
                await m.edit(content=m.content +
                             "\nThe warning could not be delivered!")

    @command()
    async def disallow(self, ctx, who: discord.User,
                       where: discord.TextChannel, what: str, *, reason: str):
        if not await is_mod(ctx.message.guild, ctx.message.author,
                            ctx.message.channel):
            await ctx.send("You need moderation permissions to do this.")
            return
        if ctx.message.guild.id != 132720341058453504:
            return
        word = reason.split(' ')[0]
        groups = re.match(r"([0-9]{1,2}d)?([0-9]{1,2}h)?([0-9]{1,2}m)?",
                          word).group(1, 2, 3)
        timeout = ""
        if groups != (None, None, None):
            delay = 0
            if groups[0] is not None:
                delay += int(groups[0][:-1])
            delay *= 24
            if groups[1] is not None:
                delay += int(groups[1][:-1])
            delay *= 60
            if groups[2] is not None:
                delay += int(groups[2][:-1])
            delay *= 60
            timeout = datetime.utcnow() + timedelta(seconds=delay)
            timeout = timeout.timestamp()

        overwrite = where.overwrites_for(who)
        if overwrite is None:
            overwrite = discord.PermissionOverwrite()

        if what == "send":
            overwrite.send_messages = False
        elif what == "react":
            overwrite.add_reactions = False
        elif what == "external":
            overwrite.external_emojis = False
        elif what == "embed":
            overwrite.embed_links = False
            overwrite.attach_files = False

        await where.set_permissions(who, overwrite=overwrite, reason=reason)
        await self.add_entry({
            "mod": ctx.message.author.id,
            "member": who.id,
            "channel": where.id,
            "verb": what,
            "kind": "disallow",
            "reason": reason,
            "expires": timeout
        })
        if timeout != "":
            delay = datetime.fromtimestamp(timeout) - datetime.utcnow()
            if delay.total_seconds() < 4 * 60 * 60 and \
                    not self.scheduled_scheduler:
                self.schedule()
            elif not self.scheduled_scheduler:
                asyncio.get_event_loop().call_later(60 * 60 * 4,
                                                    self.schedule)
                self.scheduled_scheduler = True

    @command()
    async def allow(self, ctx, who: discord.User,
                    where: discord.TextChannel, what: str, *, reason: str):
        if not await is_mod(ctx.message.guild, ctx.message.author,
                            ctx.message.channel):
            await ctx.send("You need moderation permissions to do this.")
            return
        if ctx.message.guild.id != 132720341058453504:
            return
        overwrite = where.overwrites_for(who)
        if overwrite is None:
            overwrite = discord.PermissionOverwrite()

        if what == "send":
            overwrite.send_messages = None
        elif what == "react":
            overwrite.add_reactions = None
        elif what == "external":
            overwrite.external_emojis = None
        elif what == "embed":
            overwrite.embed_links = None
            overwrite.attach_files = None
        if overwrite.is_empty():
            overwrite = None
        await where.set_permissions(who, overwrite=overwrite, reason=reason)
        await self.add_entry({
            "mod": ctx.message.author.id,
            "member": who.id,
            "channel": where.id,
            "verb": what,
            "kind": "allow",
            "reason": reason,
        })

    @group()
    async def modlog(self, ctx):
        if not await is_mod(ctx.message.guild, ctx.message.author,
                            ctx.message.channel):
            await ctx.send("You need moderation permissions to do this.")
            return
        if ctx.message.guild.id != 132720341058453504:
            return

    @modlog.command()
    async def count(self, ctx, user: discord.Member = None):
        if not await is_mod(ctx.message.guild, ctx.message.author,
                            ctx.message.channel):
            await ctx.send("You need moderation permissions to do this.")
            return
        if ctx.message.guild.id != 132720341058453504:
            return
        if user is None:
            await ctx.send(f"{len(table)} entries in database")
        else:
            entry = Query()
            count = len(table.search(entry.member == user.id))
            await ctx.send(f"{user.mention} has {count} entries in the "
                           f"database")

    @modlog.command()
    async def list(self, ctx, user: discord.Member = None):
        if user is None:
            entries = list(reversed(list(table)))[:10]
        else:
            entry = Query()
            entries = list(reversed(table.search(entry.member == user.id)))[:5]
        msg = ""
        for entry in entries:
            msg += f"<@{entry['mod']}> {entry['kind']}ed <@{entry['member']}>"
            msg += f" to {entry['verb']} in <#{entry['channel']}>: "
            msg += f"{entry['reason']}\n"
        await ctx.send(msg)


def setup(bot):
    global cog
    cog = Mod(bot)
