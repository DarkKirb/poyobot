'''A simple statistics module'''
from utils import Cog, command
from discord.ext import commands
import subprocess
import asyncio
import os
import shlex
from tabulate import tabulate


__author__ = "Charlotte D"
__license__ = "BSD-2clause"
__website__ = "https://gitlab.com/DarkKirb/poyobot/blob/master/mod/stats.py"
__version__ = "1.0"
dependencies = []


def launch_process(*args, raw=False):
    fix = lambda x: shlex.quote(x)
    if raw:
        fix = lambda x: x
    return asyncio.create_subprocess_shell(
        ' '.join([fix(x) for x in args]),
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )


async def get_output(*args, raw=False):
    process = await launch_process(*args, raw=raw)
    data = (await process.communicate())[0]
    if isinstance(data, bytes):
        return data.decode("UTF-8").strip()
    return data.strip()


def format_human(number):
    suffixes = ["", "ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi", "Yi"]
    suffix_no = 0
    while number > 1024:
        number /= 1024
        suffix_no += 1
    round_digits = 3
    if number >= 10:
        round_digits -= 1
    if number >= 100:
        round_digits -= 1
    if number >= 1000:
        round_digits -= 1
    return f"{round(number, round_digits)}{suffixes[suffix_no]}B"


class Stats(Cog):
    @command()
    async def stats(self, ctx):
        # First, get OS
        cur_os = await get_output("uname", "-s") == "FreeBSD"
        if cur_os:
            physmem = int(await get_output("sysctl", "-n", "hw.physmem"))
            usermem = int(await get_output("sysctl", "-n", "hw.usermem"))
            swap_total = int(await get_output("BLOCKSIZE=512 swapinfo | "
                                              "tail -n1 | awk '{print $2 * "
                                              "512}'", raw=True))
            swap_free = int(await get_output("BLOCKSIZE=512 swapinfo | "
                                             "tail -n1 | awk '{print $4 * "
                                             "512}'", raw=True))
            mem_str = f"RAM: {format_human(usermem)} free out of " \
                      f"{format_human(physmem)}\n"
            mem_str += f"SWAP: {format_human(swap_free)} free out of " \
                       f"{format_human(swap_total)}\n"
        else:
            mem_str = await get_output("free")

        if os.path.isfile("/sbin/zpool"):
            disk_free = int(await get_output("zpool", "get", "-Hpo", "value",
                                             "free"))
            disk_size = int(await get_output("zpool", "get", "-Hpo", "value",
                                             "size"))
            disk_str = f"{format_human(disk_free)} free out of "\
                       f"{format_human(disk_size)}"
        else:
            total = int(await get_output("df -kP |grep -v ^Filesystem |awk "
                                         "'{sum += $2} END { print sum; }  '",
                                         raw = True))
            free = int(await get_output("df -kP |grep -v ^Filesystem |awk "
                                        "'{sum += $4} END { print sum; }  '",
                                        raw = True))
            disk_str = f"{format_human(free * 1024)} free out of " \
                       f"{format_human(total * 1024)}"

        uptime = await get_output("uptime")

        # get bot stats
        no_shards = len(self.bot.shards)
        no_servers = {}
        no_servers_total = 0
        no_users = {}
        no_users_total = 0
        no_unique_users = {}
        no_unique_users_total = set()

        def get_shard_id(gid):
            return (gid >> 22) % no_shards

        for guild in self.bot.guilds:
            shard = get_shard_id(guild.id)
            if shard not in no_servers:
                no_servers[shard] = 1
                no_users[shard] = len(guild.members)
                no_unique_users[shard] = set(x.id for x in guild.members)
            else:
                no_servers[shard] += 1
                no_users[shard] += len(guild.members)
                no_unique_users[shard] |= set(x.id for x in guild.members)
            no_servers_total += 1
            no_users_total += len(guild.members)
            no_unique_users_total |= set(x.id for x in guild.members)

        data = [[k, no_servers[k], no_users[k], len(no_unique_users[k])] for
                k in no_servers.keys()]
        data += [["Total", no_servers_total,
                  no_users_total, len(no_unique_users_total)]]

        table = tabulate(data, headers=["Shard", "Server Count", "User count",
                                        "(unique)"])

        await ctx.send(f"""\
Memory:
```{mem_str}```
Disk:
```{disk_str}```
Uptime:
```{uptime}```
Discord:
```{table}```""")


def setup(bot):
    global cog
    cog = Stats(bot)
