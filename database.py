from tinydb import TinyDB, Query
from tinydb.storages import JSONStorage
from tinydb.middlewares import CachingMiddleware
from datetime import datetime

db = TinyDB('db.json')

epoch = datetime.utcfromtimestamp(0)


def gen_id():
    return int((datetime.utcnow() - epoch).total_seconds() * 1000.0)
