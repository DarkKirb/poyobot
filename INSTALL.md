# Poyobot setup instructions

## Requirements
- Python 3.6 or later
- libopus (for the audio module)
- ffmpeg (for the audio module)
- git (for the autoupdater module)
- mongodb running on localhost


## Config file
There is a single config file stored in the root directory of the bot, called config.json. The template looks like this:
```json
{
    "token": "insert bot token here",
    "database": "insert database name for poyobot here"
}
```

## Setting up the environment
```sh
python3 -m venv venv
. venv/bin/activate
pip install -r requirements.txt
```

## Running the bot
```sh
. venv/bin/activate # If you don't have a venv enabled
while true; do
python main.py
done
```
