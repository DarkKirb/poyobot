#!/usr/bin/env python3
import textwrap
import subprocess
name = input("Name of script: ")
desc = input("Description: ")
desc_str = repr(desc)
desc_str = desc_str.replace("\\n", "\n")
desc_str = '\n'.join(textwrap.wrap(desc_str, width=79))
desc_str = desc_str[0] * 2 + desc_str + desc_str[-1] * 2
git = subprocess.Popen(["git", "config", "user.name"], stdout=subprocess.PIPE)
out, err = git.communicate()
author = out.decode().strip()
deps = input("Dependencies: (with quotes and commas)")
fname = name.lower()

with open(f"mod/{fname}.py", "w") as f:
    f.write(desc_str + "\n")
    f.write(f"""from utils import Cog, command
from discord.ext import commands


__author__ = "{author}"
__license__ = "BSD-2clause"
__website__ = "https://gitlab.com/DarkKirb/poyobot/blob/master/mod/{fname}.py"
__version__ = "1.0"
dependencies = [{deps}]


class {name}(Cog):
    pass


def setup(bot):
    global cog
    cog = {name}(bot)
""")

